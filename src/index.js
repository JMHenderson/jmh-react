import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const Index = () => {
    return <div>Hello React!</div>;
}

ReactDOM.render(<Index />, document.getElementById("index"));